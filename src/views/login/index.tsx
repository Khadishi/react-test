import React, { useState, ChangeEvent, FormEvent } from "react";

const Login: React.FC = () => {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const validatePassword = (password: string): boolean => {
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    return re.test(password);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    if (validatePassword(password)) {
      // Implement API login call here
    } else {
      alert(
        "Password should contain minimum 8 characters, at least 1 uppercase, 1 lowercase, and 1 special character"
      );
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Username"
        value={username}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setUsername(e.target.value)
        }
        required
      />
      <input
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setPassword(e.target.value)
        }
        required
      />
      <button type="submit">Login</button>
    </form>
  );
};

export default Login;
